import 'package:expenselist/src/bloc/bottomnav_bloc.dart';
import 'package:expenselist/src/bloc/calendar_bloc.dart';
import 'package:expenselist/src/bloc/calendar_item_bloc.dart';
import 'package:expenselist/src/bloc/charts_bloc.dart';
import 'package:expenselist/src/bloc/inputexpense_bloc.dart';
import 'package:expenselist/src/screens/bottomnav.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class SimpleBlocDelegate extends BlocDelegate {
  @override
  void onEvent(Bloc bloc, Object event) {
    super.onEvent(bloc, event);
    print(event);
  }

  @override
  void onTransition(Bloc bloc, Transition transition) {
    super.onTransition(bloc, transition);
    print(transition);
  }

  @override
  void onError(Bloc bloc, Object error, StackTrace stacktrace) {
    super.onError(bloc, error, stacktrace);
    print(error);
  }
}

void main() {
  BlocSupervisor.delegate = SimpleBlocDelegate();
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]).then((_) {
    runApp(App());
  });
}

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider<BottomnavBloc>(
          create: (context) => BottomnavBloc(),
        ),
        BlocProvider<InputexpenseBloc>(
          create: (context) => InputexpenseBloc(),
        ),
        BlocProvider<CalendarBloc>(
          create: (context) => CalendarBloc(),
        ),
        BlocProvider<CalendarItemBloc>(
          create: (context) => CalendarItemBloc(),
        ),
        BlocProvider<ChartsBloc>(
          create: (context) => ChartsBloc(),
        ),
      ],
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Daily Expense',
        theme: ThemeData(
          primarySwatch: Colors.green,
          brightness: Brightness.light,
          primaryColor: Colors.white,
          accentColor: Colors.lightGreen,
          focusColor: Colors.black,
          scaffoldBackgroundColor: Color(0xfffcfcff),
          appBarTheme: AppBarTheme(
            elevation: 0,
            textTheme: TextTheme(
              title: TextStyle(
                color: Colors.black,
                fontSize: 18.0,
                fontWeight: FontWeight.w800,
              ),
            ),
          ),
        ),
        home: BottomnavPage(),
      ),
    );
  }
}
