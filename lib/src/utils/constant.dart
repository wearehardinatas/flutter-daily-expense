class Constant{

  static final String TYPE_NULL = ""; 
  static final String TYPE_IN = "Pemasukan"; 
  static final String TYPE_OUT = "Pengeluaran"; 

  static final List<String> types = <String>[
    Constant.TYPE_NULL,
    Constant.TYPE_IN,
    Constant.TYPE_OUT
  ];
}