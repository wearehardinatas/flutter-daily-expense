class ApiException implements Exception {
  final String msg;

  ApiException(this.msg);

  @override
  String toString() => 'ApiException: $msg';
}

class SqliteException implements Exception {
  final String msg;

  SqliteException(this.msg);

  @override
  String toString() => 'Database: $msg';
}