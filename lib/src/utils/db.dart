import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

class DBHelper {
  static DBHelper _dbHelper;
  static Database _database;
  DBHelper._createObject();
  factory DBHelper() {
    if(_dbHelper == null) {
      _dbHelper = DBHelper._createObject();
    }

    return _dbHelper;
  }

  Future<Database> initDb() async {
    String dbPath = await getDatabasesPath();
    String path = join(dbPath, 'mop.db');
    // await deleteDatabase(path);

    final Future<Database> database = openDatabase( path,
    
      onCreate: (db, version) {
        return db.execute(
          "CREATE TABLE expense(id INTEGER PRIMARY KEY AUTOINCREMENT, type TEXT, title TEXT, content TEXT, price DOUBLE, date TEXT)",
        );
      },
      version: 1,
    );

    return database;
  }

  Future<Database> get database async {
    if(_database == null) {
      _database = await initDb();
    }

    return _database;
  }
}