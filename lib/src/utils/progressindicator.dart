import 'package:flutter/material.dart';

Future<void> asyncProgressIndicator(BuildContext context) async {
  return showDialog<void>(
    context: context,
    barrierDismissible: false,
    builder: (context) {
      return Container(
        child: Center(child: CircularProgressIndicator(),),
      );
    }
  );
}