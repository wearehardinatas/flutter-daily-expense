part of 'bottomnav_bloc.dart';

abstract class BottomnavState extends Equatable {
  const BottomnavState();
  @override
  List<Object> get props => [];
}

class BottomnavInitial extends BottomnavState {}

class BottomnavHome extends BottomnavState {}
class BottomnavDate extends BottomnavState {}
class BottomnavProfile extends BottomnavState {}
class BottomnavSetting extends BottomnavState {}
