part of 'calendar_item_bloc.dart';

abstract class CalendarItemState extends Equatable {
  const CalendarItemState();
  @override
  List<Object> get props => [];
}

class CalendarItemInitial extends CalendarItemState {}
class CalendarItemLoading extends CalendarItemState {}
class CalendarItemLoaded extends CalendarItemState {
  
  final List<ExpenceModel> items;

  CalendarItemLoaded({@required this.items});

  @override
  List<Object> get props => [items];
  @override
  String toString() => 'CalendarItemLoaded { items: ${items.length} }';
}
class CalendarItemFailure extends CalendarItemState {
  final String e;

  CalendarItemFailure({@required this.e});

  @override
  List<Object> get props => [e];
  @override
  String toString() => 'CalendarItemFailure { e: $e }';
}
