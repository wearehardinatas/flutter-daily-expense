part of 'inputexpense_bloc.dart';

abstract class InputexpenseState extends Equatable {
  const InputexpenseState();
  @override
  List<Object> get props => [];
}

class InputexpenseInitial extends InputexpenseState {}
class InputexpenseSuccess extends InputexpenseState {}
class InputexpenseResetForm extends InputexpenseState {}
class InputexpenseLoading extends InputexpenseState {}
class InputexpenseFailure extends InputexpenseState {
  final String error;

  InputexpenseFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'InputexpenseFailure {error: $error}';
}
