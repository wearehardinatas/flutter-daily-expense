part of 'inputexpense_bloc.dart';

abstract class InputexpenseEvent extends Equatable {
  const InputexpenseEvent();
  @override
  List<Object> get props => [];
}

class OnSubmitButtonPressed extends InputexpenseEvent{
  final String type;
  final String title;
  final String date;
  final Map<String, dynamic> items;

  OnSubmitButtonPressed({@required this.type, @required this.title, @required this.date, @required this.items});
  
  @override
  List<Object> get props => [type, title, date, items];

  @override
  String toString() => 'OnSubmitButtonPressed {type: $type, title: $title, date: $date, items: $items}';
}

class OnUpdateButtonPressed extends InputexpenseEvent{
  // final int id;
  // final String type;
  // final String title;
  // final String date;
  // final int price;
  // final String content;

  final ExpenceModel model;

  // OnUpdateButtonPressed({@required this.id, @required this.type, @required this.title, @required this.date, @required this.price, @required this.content});
  OnUpdateButtonPressed({@required this.model});
  
  @override
  // List<Object> get props => [id, type, title, date, price, content];
  List<Object> get props => [model];

  @override
  // String toString() => 'OnUpdateButtonPressed {id: $id, type: $type, title: $title, date: $date, price: $price, content: $content}';
  String toString() => 'OnUpdateButtonPressed {model: $model}';
}

class OnResetForm extends InputexpenseEvent{
  
  @override
  List<Object> get props => [];

  @override
  String toString() => 'OnResetForm {}';
}

class OnResetFormDone extends InputexpenseEvent{
  
  @override
  List<Object> get props => [];

  @override
  String toString() => 'OnResetFormDone {}';
}

class OnCheckedClearStorage extends InputexpenseEvent{
  
  @override
  List<Object> get props => [];

  @override
  String toString() => 'OnCheckedClearStorage {}';
}

class OnRemoveItem extends InputexpenseEvent{
  
  final int id;

  OnRemoveItem({@required this.id});

  @override
  List<Object> get props => [id];

  @override
  String toString() => 'OnRemoveItem {id: $id}';
}
