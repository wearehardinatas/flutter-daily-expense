part of 'calendar_bloc.dart';

abstract class CalendarEvent extends Equatable {
  const CalendarEvent();
  
  @override
  List<Object> get props => [];
}

class OnWaitItemToRemoved extends CalendarEvent{}

class OnCalendarLoad extends CalendarEvent{
  final DateTime selectedDate;

  OnCalendarLoad({@required this.selectedDate});

  @override
  List<Object> get props => [selectedDate];
  
  @override
  String toString() => 'OnCalendarLoad {selectedDate: $selectedDate}';
}
