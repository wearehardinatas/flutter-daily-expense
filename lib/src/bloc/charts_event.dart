part of 'charts_bloc.dart';

abstract class ChartsEvent extends Equatable {
  const ChartsEvent();
  @override
  List<Object> get props => [];
}

class OnChartLoad extends ChartsEvent {}