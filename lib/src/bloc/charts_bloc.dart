import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:charts_flutter/flutter.dart' as charts;
import 'package:expenselist/src/models/chart_pie.dart';
import 'package:expenselist/src/models/chart_stacked.dart';
import 'package:expenselist/src/repo/local.dart';
import 'package:intl/intl.dart';
import 'package:meta/meta.dart';

part 'charts_event.dart';
part 'charts_state.dart';

class ChartsBloc extends Bloc<ChartsEvent, ChartsState> {
  @override
  ChartsState get initialState => ChartsInitial();

  @override
  Stream<ChartsState> mapEventToState(
    ChartsEvent event,
  ) async* {

    if(event is OnChartLoad) {
      yield ChartLoading();
      try {
        var series = await _stackedSeries();
        await Future.delayed(Duration(seconds: 1));
        var pieSeries = await _pieMonthlySeries();
        await Future.delayed(Duration(seconds: 1));
        var pieDailySeries = await _pieDailySeries();
        yield ChartLoaded(
          bar: series, 
          stacked: series, 
          pieMonthly: pieSeries, 
          pieDaily: pieDailySeries
        );
      } catch (e) {
        yield ChartFailure(error: e.toString());
      }
    }

  }

  Future<List<charts.Series<ChartStackedModel, String>>> _stackedSeries() async {
    final DateFormat formatter = DateFormat('y');
    final DateTime now = DateTime.now();
    final String year = formatter.format(now);

    LocalRepo _localRepo = LocalRepo();
    Map<String, List<ChartStackedModel>> map = await _localRepo.getYearlyStackedChart(year);
    List<charts.Series<ChartStackedModel, String>> data = [];

    map.forEach((k, v) {
      data.add(
        charts.Series<ChartStackedModel, String>(
          id: '$k',
          domainFn: (ChartStackedModel model, _) => model.month,
          measureFn: (ChartStackedModel model, _) => model.value,
          data: v,
        )
      );
    });

    return data;
  }

  Future<List<charts.Series<ChartPieModel, String>>> _pieMonthlySeries() async {
    LocalRepo _localRepo = LocalRepo();
    Map<String, ChartPieModel> maps = await _localRepo.getMonthlyPieChart(DateTime.now());
    
    List<charts.Series<ChartPieModel, String>> data = [];
    List<ChartPieModel> pies = [];
    maps.forEach((k, v) => pies.add(ChartPieModel(segment: v.segment, value: v.value)));
    
    data.add(
      charts.Series<ChartPieModel, String>(
        id: 'TypeMonthly',
        domainFn: (ChartPieModel model, _) => model.segment,
        measureFn: (ChartPieModel model, _) => model.value,
        data: pies,
      )
    );

    return data;
  }

  Future<List<charts.Series<ChartPieModel, String>>> _pieDailySeries() async {
    LocalRepo _localRepo = LocalRepo();
    Map<String, ChartPieModel> maps = await _localRepo.getDailyPieChart(DateTime.now());
    List<charts.Series<ChartPieModel, String>> data = [];
    List<ChartPieModel> pies = [];
    maps.forEach((k, v) => pies.add(ChartPieModel(segment: v.segment, value: v.value)));
    
    data.add(
      charts.Series<ChartPieModel, String>(
        id: 'TypeDaily',
        domainFn: (ChartPieModel model, _) => model.segment,
        measureFn: (ChartPieModel model, _) => model.value,
        data: pies,
      )
    );

    return data;
  }
}
