import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:expenselist/src/models/expence.dart';
import 'package:expenselist/src/repo/local.dart';
import 'package:expenselist/src/utils/exceptions.dart';
import 'package:meta/meta.dart';

part 'inputexpense_event.dart';
part 'inputexpense_state.dart';

class InputexpenseBloc extends Bloc<InputexpenseEvent, InputexpenseState> {
  
  LocalRepo localRepo = LocalRepo();

  @override
  InputexpenseState get initialState => InputexpenseInitial();

  @override
  Stream<InputexpenseState> mapEventToState(
    InputexpenseEvent event,
  ) async* {
    if(event is OnSubmitButtonPressed) {
      yield InputexpenseLoading();
      try {

        if(event.items.length > 0) {
          String title = event.title.toString();
          String date = event.date.toString();
          String type = event.type.toString();

          event.items.forEach((key, value) async {

            var content = value['content'];
            var price = value['price'];

            ExpenceModel model = ExpenceModel(
              id: null,
              title: title,
              type: type,
              date: date,
              content: content.toString(),
              price: int.parse(price)
            );
            await localRepo.insertExpense(model);
            
          });

          await Future.delayed(Duration(seconds: 2));
          yield InputexpenseSuccess();
          
        }else{
          throw SqliteException("Item(s) is required!");
        }

      } catch (e) {
        await Future.delayed(Duration(seconds: 1));
        yield InputexpenseFailure(error: e.toString());
      }
    }

    if(event is OnUpdateButtonPressed) {
      yield InputexpenseLoading();
      try {

        ExpenceModel model = event.model;
        await localRepo.updateExpense(model);
        await Future.delayed(Duration(seconds: 2));
        yield InputexpenseSuccess();

      } catch (e) {
        await Future.delayed(Duration(seconds: 1));
        yield InputexpenseFailure(error: e.toString());
      }
    }

    if(event is OnResetForm) {
      yield InputexpenseResetForm();
    }

    if(event is OnResetFormDone) {
      yield InputexpenseInitial();
    }

    if(event is OnCheckedClearStorage) {
      yield InputexpenseLoading();

      try {
        await Future.delayed(Duration(seconds: 2));
        await localRepo.clearExpense();
        yield InputexpenseSuccess();
      } catch (e) {
        await Future.delayed(Duration(seconds: 1));
        yield InputexpenseFailure(error: e.toString());
      }
    }

    if(event is OnRemoveItem) {
      yield InputexpenseLoading();
      try {
        await Future.delayed(Duration(seconds: 1));
        await localRepo.clearExpenseById(event.id);
        yield InputexpenseSuccess();
      } catch (e) {
        await Future.delayed(Duration(seconds: 1));
        yield InputexpenseFailure(error: e.toString());
      }
    }
  }
}
