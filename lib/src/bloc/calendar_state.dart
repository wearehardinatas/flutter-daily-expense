part of 'calendar_bloc.dart';

abstract class CalendarState extends Equatable {
  const CalendarState();
  @override
  List<Object> get props => [];
}

class CalendarInitial extends CalendarState {}
class CalendarLoading extends CalendarState {}
class CalendarFailure extends CalendarState {}
class CalendarLoad extends CalendarState {
  
  final DateTime startDate;
  final DateTime endDate;
  final DateTime selectedDate;
  final List<DateTime> markedDates;
  final Map<String, List<String>> markedDateTypes;

  CalendarLoad({@required this.startDate, @required this.endDate, @required this.selectedDate, @required this.markedDates, @required this.markedDateTypes});

  @override
  List<Object> get props => [startDate, endDate, selectedDate, markedDates, markedDateTypes];

  @override
  String toString() => 'CalendarLoad { startDate: $startDate, endDate: $endDate, selectedDate: $selectedDate, markedDates: ${markedDates.length} }';
}
