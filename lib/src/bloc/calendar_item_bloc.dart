import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:expenselist/src/models/expence.dart';
import 'package:expenselist/src/repo/local.dart';
import 'package:intl/intl.dart';
import 'package:meta/meta.dart';

part 'calendar_item_event.dart';
part 'calendar_item_state.dart';

class CalendarItemBloc extends Bloc<CalendarItemEvent, CalendarItemState> {

  LocalRepo _localRepo = LocalRepo();

  @override
  CalendarItemState get initialState => CalendarItemInitial();

  @override
  Stream<CalendarItemState> mapEventToState(
    CalendarItemEvent event,
  ) async* {

    if(event is OnCalendarItemLoad) {
      yield CalendarItemLoading();
      try {
        // await Future.delayed(Duration(seconds: 1));
        final formatter = DateFormat('yyyy-MM-dd');
        final getMarkedItemsByDate = await _localRepo.getMarkedItemsByDate(formatter.format(event.date).toString());
        yield CalendarItemLoaded(items: getMarkedItemsByDate);
      } catch (e) {
        yield CalendarItemFailure(e: e.toString());
      }
    }

  }
}
