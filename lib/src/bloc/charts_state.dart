part of 'charts_bloc.dart';

abstract class ChartsState extends Equatable {
  const ChartsState();
  @override
  List<Object> get props => [];
}

class ChartsInitial extends ChartsState {}
class ChartLoading extends ChartsState {}
class ChartLoaded extends ChartsState {

  final List<charts.Series<ChartStackedModel, String>> bar;
  final List<charts.Series<ChartStackedModel, String>> stacked;
  final List<charts.Series<ChartPieModel, String>> pieMonthly;
  final List<charts.Series<ChartPieModel, String>> pieDaily;

  ChartLoaded({@required this.bar, @required this.stacked, @required this.pieMonthly, @required this.pieDaily});

  @override
  List<Object> get props => [bar, stacked, pieMonthly, pieDaily];
  @override
  String toString() => 'ChartLoaded {bar: ${bar.length}, stacked: ${stacked.length}, pieMonthly: ${pieMonthly.length}, pieDaily: ${pieDaily.length}}';
}
class ChartFailure extends ChartsState {
  final String error;

  ChartFailure({@required this.error});
  @override
  List<Object> get props => [error];
  @override
  String toString() => 'ChartFailure {error: $error}';
}

