part of 'calendar_item_bloc.dart';

abstract class CalendarItemEvent extends Equatable {
  const CalendarItemEvent();
  @override
  List<Object> get props => [];
}

class OnCalendarItemLoad extends CalendarItemEvent {
  final DateTime date;

  OnCalendarItemLoad({@required this.date});

  @override
  List<Object> get props => [date];
  @override
  String toString() => 'OnCalendarItemLoad { date: $date }';
}
