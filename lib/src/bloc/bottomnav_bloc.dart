import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:expenselist/src/models/chart_stacked.dart';
import 'package:expenselist/src/models/expence.dart';
import 'package:expenselist/src/repo/local.dart';
import 'package:meta/meta.dart';

part 'bottomnav_event.dart';
part 'bottomnav_state.dart';

class BottomnavBloc extends Bloc<BottomnavEvent, BottomnavState> {

  LocalRepo _localRepo = LocalRepo();
  int currentIndex = 0;
  List<ExpenceModel> models;

  @override
  BottomnavState get initialState => BottomnavHome();

  @override
  Stream<BottomnavState> mapEventToState(
    BottomnavEvent event,
  ) async* {
    
    models = await _localRepo.allexpense();

    if(event is OnBottomnavChange) {
      
      this.currentIndex = event.index;

      switch (event.index) {
        case 1:
          yield BottomnavDate();
          break;
        case 2:
          yield BottomnavSetting();
          break;
        default:
          yield BottomnavHome();
          break;
      }
    }
  }
}
