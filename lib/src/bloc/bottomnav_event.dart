part of 'bottomnav_bloc.dart';

abstract class BottomnavEvent extends Equatable {
  const BottomnavEvent();
  @override
  List<Object> get props => [];
}

class OnBottomnavChange extends BottomnavEvent {
  final int index;

  OnBottomnavChange({@required this.index});
  
  @override
  List<Object> get props => [index];
}
