import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:expenselist/src/models/expence.dart';
import 'package:expenselist/src/repo/local.dart';
import 'package:intl/intl.dart';
import 'package:meta/meta.dart';

part 'calendar_event.dart';
part 'calendar_state.dart';

class CalendarBloc extends Bloc<CalendarEvent, CalendarState> {

  LocalRepo _localRepo = LocalRepo();
  DateTime startDate = DateTime.now().subtract(Duration(days: 365));
  DateTime endDate = DateTime.now().add(Duration(days: (365*100)));

  @override
  CalendarState get initialState => CalendarInitial();

  @override
  Stream<CalendarState> mapEventToState(
    CalendarEvent event,
  ) async* {
    
    final currentState = state;

    if(event is OnCalendarLoad) {
      yield CalendarLoading();
      try {
        DateTime d;
        if (event.selectedDate == null) {
          if(currentState is CalendarLoad) {
            d = currentState.selectedDate;
          }else{
            d = DateTime.now();
          }
        }else{
          d = event.selectedDate;
        }
        
        final getMarkedTypes = await _localRepo.getMarkedTypes();
        final getMarkedDates = await _localRepo.getMarkedDates();

        yield CalendarLoad(
          startDate: startDate,
          endDate: endDate,
          selectedDate: d,
          markedDates: getMarkedDates,
          markedDateTypes: getMarkedTypes
        );
      } catch (e) {
        yield CalendarFailure();
      }
    } 

  }
}
