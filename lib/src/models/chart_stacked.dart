import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

class ChartStackedModel extends Equatable
{
  final String month;
  final int value;

  ChartStackedModel({@required this.month, @required this.value});

  @override
  List<Object> get props => [month, value];

  Map<String, dynamic> toJson() => {
    "month": this.month,
    "value": this.value,
  };

  @override
  String toString() => 'ChartStackedModel { month: $month, value: $value }';
  
}