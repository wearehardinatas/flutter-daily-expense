import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

class ChartPieModel extends Equatable
{
  final String segment;
  final int value;

  ChartPieModel({@required this.segment, @required this.value});

  @override
  List<Object> get props => [segment, value];

  Map<String, dynamic> toJson() => {
    "segment": this.segment,
    "value": this.value,
  };

  @override
  String toString() => 'ChartPieModel { segment: $segment, value: $value }';
  
}