import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

class ExpenceModel extends Equatable
{
  final int id;
  final String type;
  final String title;
  final String content;
  final int price;
  final String date;

  ExpenceModel({
    @required this.id, 
    @required this.type, 
    @required this.title, 
    @required this.content, 
    @required this.price,
    @required this.date
  });

  @override
  List<Object> get props => [
    id, 
    type, 
    title, 
    content, 
    price,
    date
  ];

  Map<String, dynamic> toJson() => {
    "id": this.id,
    "type": this.type,
    "title": this.title,
    "content": this.content,
    "price": this.price,
    "date": this.date
  };
  
  @override
  String toString() => 'ExpenceModel {id: $id, type: $type, title: $title, content: $content, price: $price, date: $date}';
}