import 'package:expenselist/src/bloc/inputexpense_bloc.dart';
import 'package:expenselist/src/utils/constant.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';

class FormExpense extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Add Expense List"),
      ),
      body: BlocListener<InputexpenseBloc, InputexpenseState>(
        bloc: BlocProvider.of<InputexpenseBloc>(context),
        listener: (context, state) {

          // if(state is InputexpenseLoading) {
          //   asyncProgressIndicator(context);
          // }
          
          if(state is InputexpenseFailure) {
            // Navigator.of(context).pop();
            Scaffold.of(context).showSnackBar(
              SnackBar(
                content: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Icon(Icons.error_outline),
                    Expanded(
                      child: Text(state.error, maxLines: 4,
                        overflow: TextOverflow.ellipsis,
                        // textDirection: TextDirection.ltr,
                        textAlign: TextAlign.left
                      ),
                    )
                  ],
                ),
                backgroundColor: Colors.red,
                behavior: SnackBarBehavior.floating,
              ),
            );
          }

          if(state is InputexpenseSuccess) {
            Scaffold.of(context).showSnackBar(
              SnackBar(
                content: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Icon(Icons.check_circle_outline),
                    Text('Save success.')
                  ],
                ),
                backgroundColor: Colors.green,
                behavior: SnackBarBehavior.floating,
                
              ),
            )
            .closed
            .then((SnackBarClosedReason reason) {
              if(reason == SnackBarClosedReason.timeout) {
                Navigator.of(context).pop();
              }else{
                BlocProvider.of<InputexpenseBloc>(context)..add(OnResetForm());
              }
            });

          }
        },
        child: SafeArea(
          top: false,
          bottom: false,
          child: ListView(
            padding: EdgeInsets.symmetric(horizontal: 24.0),
            children: <Widget>[
              SizedBox(height: 10.0),
              AddForm(),
            ],
          ),
        ),
      ),
    
    );
  }
}

class AddForm extends StatefulWidget {
  @override
  _AddFormState createState() => _AddFormState();
}

class _AddFormState extends State<AddForm> {
  final _typeController = TextEditingController();
  final _titleController = TextEditingController();
  final _dateController = TextEditingController();
  
  final List<String> _types = Constant.types;
  
  String _type = '';

  List<Widget> _contentWidgets = List<Widget>();
  Map<String,TextEditingController> _contentControllers = {};
  Map<String,TextEditingController> _priceControllers = {};
  InputexpenseBloc _inputexpenseBloc;

  DateTime _selectedDate = DateTime.now();

  Future<Null> _selectDate(BuildContext context) async {
    var formatter = DateFormat('yyyy-MM-dd');
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: _selectedDate,
        firstDate: DateTime(2015, 8),
        lastDate: DateTime(2101)
    );
    if (picked != null)
      setState(() {
        _selectedDate = picked;
        _dateController.text = formatter.format(picked).toString();
      });
  }

  @override
  void initState() {
    super.initState();
    _inputexpenseBloc = BlocProvider.of<InputexpenseBloc>(context);
  }

  void _onSave() {
    Map<String, dynamic> _items = {};

    _contentControllers.forEach((k, v) {
      String price = _priceControllers[k].text;
      Map<String,dynamic> attr = Map<String,dynamic>();
      attr['content'] = v.text.toString();
      attr['price'] = price;
      _items[k] = attr;
    });

    _inputexpenseBloc.add(OnSubmitButtonPressed(
      title: _titleController.text.toString(),
      type: _type.toString(),
      date: _dateController.text.toString(),
      items: _items
    ));
  }

  void _resetForm() {
    print("Reset Form!");
    _titleController.text = '';
    _dateController.text = '';
    _contentControllers = {};
    _priceControllers = {};
    _contentWidgets = [];
    SchedulerBinding.instance.addPostFrameCallback((_) {
      setState(() {
        _type = '';
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    
    return BlocBuilder<InputexpenseBloc, InputexpenseState>(
      bloc: _inputexpenseBloc,
      builder: (context, state) {
        if(state is InputexpenseResetForm) {
          _resetForm();
          _inputexpenseBloc..add(OnResetFormDone());
        }
        return Form(
          child: Column(
            children: <Widget>[
              TextFormField(
                decoration: InputDecoration(
                  isDense: true,
                  contentPadding: EdgeInsets.all(10),
                  labelText: 'Date', 
                  filled: true, 
                  border: OutlineInputBorder(borderRadius: BorderRadius.circular(5.0))
                ),
                onTap: () => _selectDate(context),
                controller: _dateController,
                readOnly: true,
              ),
              SizedBox(height: 12.0),
              FormField<String>(
                builder: (FormFieldState<String> state) {
                  return InputDecorator(
                    decoration: InputDecoration(
                      isDense: true,
                      contentPadding: EdgeInsets.all(10),
                      errorStyle: TextStyle(color: Colors.redAccent, fontSize: 16.0),
                      hintText: 'Please select type',
                      border: OutlineInputBorder(borderRadius: BorderRadius.circular(5.0))),
                    isEmpty: _type == '',
                    child: DropdownButtonHideUnderline(
                      child: DropdownButton<String>(
                        value: _type??'',
                        isDense: true,
                        onChanged: (String newValue) {
                          setState(() {
                            _type = newValue;
                            state.didChange(newValue);
                          });
                        },
                        items: _types.map((String value) {
                          return DropdownMenuItem<String>(
                            value: value,
                            child: Text(value),
                          );
                        }).toList(),
                      ),
                    ),
                  );
                },
              ),
              SizedBox(height: 12.0),
              TextFormField(
                decoration: InputDecoration(
                  isDense: true,
                  contentPadding: EdgeInsets.all(10),
                  labelText: 'Title', 
                  filled: true, 
                  border: OutlineInputBorder(borderRadius: BorderRadius.circular(5.0))
                ),
                controller: _titleController,
              ),
              Padding(
                padding: EdgeInsets.only(right: 0.0),
                child: Column(
                  children: List.generate(_contentWidgets.length, (i) {
                    return _contentWidgets[i];
                  }),
                )
              ),
              SizedBox(height: 12.0),
              
              ButtonBar(
                children: <Widget>[
                  FlatButton(
                    // color: Colors.green,
                    textColor: Colors.blue,
                    onPressed: () {
                      setState(() {
                        // _contentWidgets.add(Container(child: Center(child: Text("Item"),),));
                        int index = _contentWidgets.length;
                        _contentWidgets.add(
                          Column(
                            key: Key('col-$index'),
                            children: <Widget>[
                              SizedBox(height: 12.0),
                              _fieldContent('col-$index')
                            ],
                          )
                        );
                        
                      });
                    },
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Icon(Icons.add_circle_outline),
                        Text('Add Item')
                      ],
                    ),
                  ),
                  FlatButton(
                    color: Colors.green,
                    textColor: Colors.white,
                    onPressed: state is! InputexpenseLoading ? _onSave : null,
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Icon(Icons.send),
                        Text('Save')
                      ],
                    ),
                  ),
                  FlatButton(
                    color: Colors.grey,
                    textColor: Colors.white,
                    onPressed: state is! InputexpenseLoading ? _resetForm : null,
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Icon(Icons.replay),
                        Text('Reset')
                      ],
                    ),
                  )
                ],
              ),

              // REMOVE TO LISTENER
              SizedBox(height: 12.0),
              Container(
                child: state is InputexpenseLoading
                    ? CircularProgressIndicator()
                    : null,
              ),
            ],
          ),
        );
      },
    );
  }

  Widget _fieldContent(String indexKey) {
    var _contentController = new TextEditingController();
    var _priceController = new TextEditingController();
    
    _contentControllers.putIfAbsent(indexKey, () => _contentController);
    _priceControllers.putIfAbsent(indexKey, () => _priceController);

    return Row(
      key: Key(indexKey),
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Flexible(
          child: Padding(
            padding: EdgeInsets.only(right: 0.0),
            child: TextFormField(
              controller: _contentController,
              decoration: InputDecoration(
                labelText: 'Item', filled: true,
                isDense: true,
                contentPadding: EdgeInsets.all(10),
                border: OutlineInputBorder(borderRadius: BorderRadius.circular(5.0))
              ),
            ),
          ),
        ),
        Flexible(
          child: Padding(
            padding: EdgeInsets.only(right: 0.0),
            child: TextFormField(
              controller: _priceController,
              keyboardType: TextInputType.number,
              decoration: InputDecoration(
                labelText: 'Price', 
                filled: true,
                isDense: true,
                contentPadding: EdgeInsets.all(10),
                border: OutlineInputBorder(borderRadius: BorderRadius.circular(5.0))
              ),
              inputFormatters: [
                WhitelistingTextInputFormatter.digitsOnly
              ],
            ),
          ),
        ),
        GestureDetector(
          child: Icon(Icons.delete_outline, color: Colors.redAccent,),
          onTap: () {
            setState(() {
              _contentWidgets.removeWhere((i) => i.key == Key(indexKey));
              _priceControllers.removeWhere((k, v) => k == indexKey);
              _contentControllers.removeWhere((k, v) => k == indexKey);
            });
          },
        ),
      ],
    );
  }
}
