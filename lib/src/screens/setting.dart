import 'package:expenselist/src/bloc/inputexpense_bloc.dart';
import 'package:expenselist/src/screens/about.dart';
import 'package:expenselist/src/utils/pageroute.dart';
import 'package:expenselist/src/utils/progressindicator.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class SettingPage extends StatefulWidget {
  @override
  _SettingPageState createState() => _SettingPageState();
}

class _SettingPageState extends State<SettingPage> {
  InputexpenseBloc _inputexpenseBloc;
  bool _checked1;
  bool _clearStorage;

  @override
  void initState() {
    super.initState();
    _inputexpenseBloc = InputexpenseBloc();
    _checked1 = false;
    _clearStorage = false;
  }

  Future<void> _asyncConfirmDialog(BuildContext context) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button for close dialog!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Clear Storage?'),
          content: const Text(
              'This will reset your data in this device.'),
          actions: <Widget>[
            FlatButton(
              child: const Text('Cancel'),
              onPressed: () {
                setState(() {
                  _clearStorage = false;
                });
                Navigator.of(context).pop();
              },
            ),
            FlatButton(
              child: const Text('Accept', style: TextStyle(color: Colors.red),),
              onPressed: () {
                _inputexpenseBloc.add(OnCheckedClearStorage());
                Navigator.of(context).pop();
              },
            )
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: MultiBlocListener(
        listeners: [
          BlocListener<InputexpenseBloc, InputexpenseState>(
            bloc: _inputexpenseBloc,
            listener: (context, state) {
              if(state is InputexpenseFailure) {
                Scaffold.of(context).showSnackBar(
                  SnackBar(
                    content: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Icon(Icons.error_outline),
                        Expanded(
                          child: Text(state.error, maxLines: 4,
                            overflow: TextOverflow.ellipsis,
                            textDirection: TextDirection.ltr,
                            textAlign: TextAlign.left
                          ),
                        )
                      ],
                    ),
                    backgroundColor: Colors.red,
                    behavior: SnackBarBehavior.floating,
                  ),
                );

                Navigator.of(context).pop();
              }

              if(state is InputexpenseLoading) {
                asyncProgressIndicator(context);
              }

              if(state is InputexpenseSuccess) {
                Navigator.of(context).pop();
              }

            },
          )
        ],
        child: ListView(
          children: <Widget>[
            ListTile(
              title: Text("This are list of all Preferences"),
              subtitle: Text("Use wisely :D"),
              onTap: (){},
            ),
            CheckboxListTile(
              value: _checked1,
              title: Text("Reminder for limit of income"),
              onChanged: (value) {
                setState(() {
                  _checked1 = value;
                });
              },
            ),
            SwitchListTile(
              title: const Text('Clear Storage'),
              value: _clearStorage,
              onChanged: (bool value) async { 
                
                setState(() {
                  _clearStorage = value;
                });

                if(value) {
                  _asyncConfirmDialog(context); 
                  return;
                }

              },
              secondary: const Icon(Icons.delete_forever),
            ),
            ListTile(
              leading: Icon(Icons.alternate_email),
              title: Text("About this software"),
              onTap: (){
                // Navigator.of(context).push(
                //   pageRoute(AboutPage())
                // );
                showDialog(
                  context: context,
                  builder: (BuildContext context) => _buildAboutDialog(context),
                );
              },
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildAboutDialog(BuildContext context) {
    return new AlertDialog(
      title: Center(
        child: Text('بِسْمِ اللّهِ الرَّحْمَنِ الرَّحِيْ', 
          style: TextStyle(
            fontWeight: FontWeight.bold
          ),
        )
      ),
      content: AboutPage(),
      actions: <Widget>[
        new FlatButton(
          onPressed: () {
            Navigator.of(context).pop();
          },
          // textColor: Theme.of(context).primaryColor,
          child: const Text('Close'),
        ),
      ],
    );
  }
}