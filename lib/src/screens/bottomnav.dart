import 'package:expenselist/src/bloc/bottomnav_bloc.dart';
import 'package:expenselist/src/screens/calendar.dart';
import 'package:expenselist/src/screens/home.dart';
import 'package:expenselist/src/screens/setting.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class BottomnavPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final BottomnavBloc _bottomNavBloc = BlocProvider.of<BottomnavBloc>(context);
    return Scaffold(
      body: BlocListener<BottomnavBloc, BottomnavState>(
        listener: (context, state) {
          if(state is BottomnavSetting) {
            
            print("List: ${_bottomNavBloc.models}");

            Scaffold.of(context).showSnackBar(
              SnackBar(
                content: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Icon(Icons.error_outline),
                    Text('You have ${_bottomNavBloc.models.length} data in storage.')
                  ],
                ),
                backgroundColor: Colors.blueAccent[100],
                behavior: SnackBarBehavior.floating,
              ),
            )
            .closed
            .then((SnackBarClosedReason reason) {
              print("Reason: $reason");
            });
          }
        },
        child: BlocBuilder<BottomnavBloc, BottomnavState>(
          bloc: _bottomNavBloc,
          builder: (context, state) {
            Widget widget = Container(
              child: Center(
                child: CircularProgressIndicator(),
              ),
            );

            if(state is BottomnavHome) {
              widget = HomePage();
            }

            if(state is BottomnavDate) {
              widget = CalendarPage();
            }

            if(state is BottomnavSetting) {
              widget = SettingPage();
            }

            return widget;
          },
        ),
      ),
      bottomNavigationBar: Theme(
        data: Theme.of(context).copyWith(
          canvasColor: Theme.of(context).primaryColor,
          primaryColor: Theme.of(context).accentColor,
          textTheme: Theme
              .of(context)
              .textTheme
              .copyWith(caption: TextStyle(color: Colors.grey[500]),
          ),
        ),
        child: BlocBuilder<BottomnavBloc, BottomnavState>(
          builder: (context, state) {
            return BottomNavigationBar(
              type: BottomNavigationBarType.fixed,
              onTap: (index) => _bottomNavBloc.add(OnBottomnavChange(index: index)),
              currentIndex: _bottomNavBloc.currentIndex,
              items: <BottomNavigationBarItem>[
                BottomNavigationBarItem(
                  icon: Icon(
                    Icons.home
                  ),
                  // title: Container(height: 0.0), // for empty title
                  title: Text("Home"),
                ),

                BottomNavigationBarItem(
                  icon: Icon(
                    Icons.calendar_today,
                  ),
                  // title: Container(height: 0.0), // for empty title
                  title: Text("Calendar")
                ),

                BottomNavigationBarItem(
                  icon: Icon(
                    Icons.person_outline,
                  ),
                  // title: Container(height: 0.0), // for empty title
                  title: Text("Setting")
                ),
              ],
            );
          },
        ),
      )
    );
  }
}