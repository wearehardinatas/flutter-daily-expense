import 'package:expenselist/src/bloc/inputexpense_bloc.dart';
import 'package:expenselist/src/models/expence.dart';
import 'package:expenselist/src/utils/constant.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';

class UpdateExpense extends StatelessWidget {
  
  final ExpenceModel model;

  const UpdateExpense({Key key, this.model}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("${model.content}"),
      ),
      body: BlocListener<InputexpenseBloc, InputexpenseState>(
        bloc: BlocProvider.of<InputexpenseBloc>(context),
        listener: (context, state) {

          // if(state is InputexpenseLoading) {
          //   asyncProgressIndicator(context);
          // }
          
          if(state is InputexpenseFailure) {
            // Navigator.of(context).pop();
            Scaffold.of(context).showSnackBar(
              SnackBar(
                content: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Icon(Icons.error_outline),
                    Expanded(
                      child: Text(state.error, maxLines: 4,
                        overflow: TextOverflow.ellipsis,
                        // textDirection: TextDirection.ltr,
                        textAlign: TextAlign.left
                      ),
                    )
                  ],
                ),
                backgroundColor: Colors.red,
                behavior: SnackBarBehavior.floating,
              ),
            );
          }

          if(state is InputexpenseSuccess) {
            Scaffold.of(context).showSnackBar(
              SnackBar(
                content: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Icon(Icons.check_circle_outline),
                    Text('Save success.')
                  ],
                ),
                backgroundColor: Colors.green,
                behavior: SnackBarBehavior.floating,
                
              ),
            );

          }
        },
        child: SafeArea(
          top: false,
          bottom: false,
          child: ListView(
            padding: EdgeInsets.symmetric(horizontal: 24.0),
            children: <Widget>[
              SizedBox(height: 10.0),
              EditForm(model: model),
            ],
          ),
        ),
      ),
    
    );
  }
}

class EditForm extends StatefulWidget {
  
  final ExpenceModel model;

  const EditForm({Key key, this.model}) : super(key: key);

  @override
  _EditFormState createState() => _EditFormState();
}

class _EditFormState extends State<EditForm> {
  final _typeController = TextEditingController();
  final _titleController = TextEditingController();
  final _dateController = TextEditingController();
  final _contentController = TextEditingController();
  final _priceController = TextEditingController();
  
  final List<String> _types = Constant.types;
  
  String _type = '';

  List<Widget> _contentWidgets = List<Widget>();
  Map<String,TextEditingController> _contentControllers = {};
  Map<String,TextEditingController> _priceControllers = {};
  InputexpenseBloc _inputexpenseBloc;

  DateTime _selectedDate = DateTime.now();

  Future<Null> _selectDate(BuildContext context) async {
    var formatter = DateFormat('yyyy-MM-dd');
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: _selectedDate,
        firstDate: DateTime(2015, 8),
        lastDate: DateTime(2101)
    );
    if (picked != null)
      setState(() {
        _selectedDate = picked;
        _dateController.text = formatter.format(picked).toString();
      });
  }

  @override
  void initState() {
    super.initState();
    _inputexpenseBloc = BlocProvider.of<InputexpenseBloc>(context);
    _titleController.text = widget.model.title;
    _dateController.text = widget.model.date;
    _contentController.text = widget.model.content;
    _priceController.text = widget.model.price.toString();
    _type = widget.model.type;
  }

  void _onSave() {
    ExpenceModel model = ExpenceModel(
      id: widget.model.id,
      title: _titleController.text.toString(),
      type: _type.toString(),
      date: _dateController.text.toString(),
      content: _contentController.text.toString(),
      price: int.parse(_priceController.text)
    );
    _inputexpenseBloc..add(OnUpdateButtonPressed(
      model: model
    ));
  }

  void _resetForm() {
    print("Reset Form!");
    _titleController.text = '';
    _dateController.text = '';
    _contentController.text = '';
    _priceController.text = '';
  }

  @override
  Widget build(BuildContext context) {
    
    return BlocBuilder<InputexpenseBloc, InputexpenseState>(
      bloc: _inputexpenseBloc,
      builder: (context, state) {
        if(state is InputexpenseResetForm) {
          _resetForm();
          _inputexpenseBloc..add(OnResetFormDone());
        }
        return Form(
          child: Column(
            children: <Widget>[
              TextFormField(
                decoration: InputDecoration(
                  isDense: true,
                  contentPadding: EdgeInsets.all(10),
                  labelText: 'Date', 
                  filled: true, 
                  border: OutlineInputBorder(borderRadius: BorderRadius.circular(5.0))
                ),
                onTap: () => _selectDate(context),
                controller: _dateController,
                readOnly: true,
              ),
              SizedBox(height: 12.0),
              FormField<String>(
                builder: (FormFieldState<String> state) {
                  return InputDecorator(
                    decoration: InputDecoration(
                      isDense: true,
                      contentPadding: EdgeInsets.all(10),
                      errorStyle: TextStyle(color: Colors.redAccent, fontSize: 16.0),
                      hintText: 'Please select type',
                      border: OutlineInputBorder(borderRadius: BorderRadius.circular(5.0))),
                    isEmpty: _type == '',
                    child: DropdownButtonHideUnderline(
                      child: DropdownButton<String>(
                        value: _type??'',
                        isDense: true,
                        onChanged: (String newValue) {
                          setState(() {
                            _type = newValue;
                            state.didChange(newValue);
                          });
                        },
                        items: _types.map((String value) {
                          return DropdownMenuItem<String>(
                            value: value,
                            child: Text(value),
                          );
                        }).toList(),
                      ),
                    ),
                  );
                },
              ),
              SizedBox(height: 12.0),
              TextFormField(
                decoration: InputDecoration(
                  isDense: true,
                  contentPadding: EdgeInsets.all(10),
                  labelText: 'Title', 
                  filled: true, 
                  border: OutlineInputBorder(borderRadius: BorderRadius.circular(5.0))
                ),
                controller: _titleController,
              ),
              SizedBox(height: 12.0),
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Flexible(
                    child: Padding(
                      padding: EdgeInsets.only(right: 0.0),
                      child: TextFormField(
                        controller: _contentController,
                        decoration: InputDecoration(
                          labelText: 'Item', filled: true,
                          isDense: true,
                          contentPadding: EdgeInsets.all(10),
                          border: OutlineInputBorder(borderRadius: BorderRadius.circular(5.0))
                        ),
                      ),
                    ),
                  ),
                  Flexible(
                    child: Padding(
                      padding: EdgeInsets.only(right: 0.0),
                      child: TextFormField(
                        controller: _priceController,
                        keyboardType: TextInputType.number,
                        decoration: InputDecoration(
                          labelText: 'Price', 
                          filled: true,
                          isDense: true,
                          contentPadding: EdgeInsets.all(10),
                          border: OutlineInputBorder(borderRadius: BorderRadius.circular(5.0))
                        ),
                        inputFormatters: [
                          WhitelistingTextInputFormatter.digitsOnly
                        ],
                      ),
                    ),
                  ),
                ],
              ),
              
              SizedBox(height: 12.0),
              
              ButtonBar(
                children: <Widget>[
                  FlatButton(
                    color: Colors.green,
                    textColor: Colors.white,
                    onPressed: state is! InputexpenseLoading ? _onSave : null,
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Icon(Icons.send),
                        Text('Update')
                      ],
                    ),
                  ),
                  FlatButton(
                    color: Colors.grey,
                    textColor: Colors.white,
                    onPressed: state is! InputexpenseLoading ? _resetForm : null,
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Icon(Icons.replay),
                        Text('Reset')
                      ],
                    ),
                  )
                ],
              ),

              SizedBox(height: 12.0),
              Container(
                child: state is InputexpenseLoading
                    ? CircularProgressIndicator()
                    : null,
              )
            ],
          ),
        );
      },
    );
  }

}
