import 'dart:async';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

class AboutPage extends StatefulWidget {
  @override
  _AboutPageState createState() => _AboutPageState();
}

class _AboutPageState extends State<AboutPage> {
  
  static const String flutterUrl = 'https://wearehardinata.com/';
  static const String githubUrl = 'http://wearehardinata.com';
  TapGestureRecognizer _flutterTapRecognizer;
  TapGestureRecognizer _githubTapRecognizer;

  static const TextStyle linkStyle = const TextStyle(
    color: Colors.blue,
    decoration: TextDecoration.underline,
  );

  @override
  void initState() {
    super.initState();
    _flutterTapRecognizer = new TapGestureRecognizer()..onTap = () => _openUrl(flutterUrl);
    _githubTapRecognizer = new TapGestureRecognizer()..onTap = () => _openUrl(githubUrl);
  }

  @override
  void dispose() {
    _flutterTapRecognizer.dispose();
    _githubTapRecognizer.dispose();
    super.dispose();
  }

  void _openUrl(String url) async {
    // Close the about dialog.
    Navigator.pop(context);

    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  Widget _buildAboutText() {
    return new RichText(
      text: new TextSpan(
        text: 'Segala puji hanya bagi Allah swt.\n\nAlhamdulillah, shalawat serta salam semoga senantiasa dilimpahkan kepada Nabi Muhammad saw, keluarga dan sahabatnya.\n\n',
        style: const TextStyle(color: Colors.black87),
        children: <TextSpan>[
          const TextSpan(text: 'Alhamdulillah atas izin Allah swt, '),
          new TextSpan(
            text: 'Kami',
            recognizer: _githubTapRecognizer,
            style: linkStyle,
          ),
          const TextSpan(
            text: ' telah berhasil membuat aplikasi sederhana untuk membantu '
                'pencatatan uang keluar dan masuk sehari-hari.\n\n',
          ),
          const TextSpan(
            text: 'Terima kasih untuk Allah swt, keluarga (Istri & anak-ku, i lope yu pul), '
            'rekan-rekan, dan para pengembang yang telah membuat'
            ' library pendukung (open source) dan digunakan pada aplikasi ini.\n\n',
          ),
          TextSpan(text: 'Semoga aplikasi ini bisa berguna dan bermanfaat.'),
        ],
      ),
    );
  }

  Widget _buildLogoAttribution() {
    return new Padding(
      padding: const EdgeInsets.only(top: 16.0),
      child: new Row(
        children: <Widget>[
          new Padding(
            padding: const EdgeInsets.only(top: 0.0),
            child: InkWell(
              onTap: () => _openUrl(flutterUrl),
              child: Image.asset(
                "assets/logo.png",
                width: 32.0,
              ),
            ),
          ),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.only(left: 12.0),
              child: RichText(
                text: TextSpan(
                  text: 'Klik disini untuk informasi menarik lainnya.',
                  recognizer: _githubTapRecognizer,
                  style: TextStyle(
                    color: Colors.black87
                  ),
                ),
              )
            )
          ),
        ],
      ),
    );
  }

  Widget _buildCritics() {
    return new Padding(
      padding: const EdgeInsets.only(top: 16.0),
      child: new Row(
        children: <Widget>[
          new Padding(
            padding: const EdgeInsets.only(top: 0.0),
            child: InkWell(
              onTap: () => _openUrl(flutterUrl),
              child: Image.asset(
                "assets/logo.png",
                width: 32.0,
              ),
            ),
          ),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.only(left: 12.0),
              child: RichText(
                text: TextSpan(
                  text: 'Kritik dan saran.',
                  recognizer: _githubTapRecognizer,
                  style: TextStyle(
                    color: Colors.black87
                  ),
                ),
              )
            )
          ),
        ],
      ),
    );
  }
  
  @override
  Widget build(BuildContext context) {
    return ListView(
      // mainAxisSize: MainAxisSize.min,
      // crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        _buildAboutText(),
        _buildLogoAttribution(),
        _buildCritics(),
      ],
    );
  }
}

class AboutPages extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("About this software"),
      ),
      body: Container(
        margin: EdgeInsets.all(10.0),
        child: Material(
          elevation: 4.0,
          borderRadius: BorderRadius.circular(6.0),
          child: ListView(
            children: <Widget>[
              Hero(
                tag: Text(""),
                child: Image.asset("assets/logo.png"),
              )
            ],
          ),
        ),
      )
    );
  }
}