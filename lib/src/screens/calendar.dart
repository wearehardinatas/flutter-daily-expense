import 'package:calendar_strip/calendar_strip.dart';
import 'package:expenselist/src/bloc/calendar_bloc.dart';
import 'package:expenselist/src/bloc/calendar_item_bloc.dart';
import 'package:expenselist/src/bloc/inputexpense_bloc.dart';
import 'package:expenselist/src/models/expence.dart';
import 'package:expenselist/src/screens/form_expense.dart';
import 'package:expenselist/src/screens/update_expense.dart';
import 'package:expenselist/src/utils/constant.dart';
import 'package:expenselist/src/utils/pageroute.dart';
import 'package:expenselist/src/utils/progressindicator.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';

class CalendarPage extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    
    DateTime defaultSelectedDate = DateTime.now();
    final CalendarBloc _calendarBloc = BlocProvider.of<CalendarBloc>(context) ..add(OnCalendarLoad(
      selectedDate: defaultSelectedDate
    ));
    final CalendarItemBloc _calendarItemBloc = BlocProvider.of<CalendarItemBloc>(context);
    final InputexpenseBloc _inputBloc = BlocProvider.of<InputexpenseBloc>(context);
    
    void onSelect(date) {
      _calendarBloc ..add(OnCalendarLoad(
        selectedDate: date
      ));
    }

    Widget _monthNameWidget(monthName) {
      return Container(
        child: Text(monthName,
          style: TextStyle(fontSize: 17, fontWeight: FontWeight.w600, color: Colors.black87, fontStyle: FontStyle.normal)
        ),
        padding: EdgeInsets.only(top: 8, bottom: 4),
      );
    }

    Widget getMarkedIndicatorWidget(CalendarLoad state, DateTime date) {
      final formatter = DateFormat('yyyy-MM-dd');
      final d = formatter.format(date);
      final types = state.markedDateTypes;
      int l = 0;
      List<Widget> w = [];
      Widget red = Container(
        margin: EdgeInsets.only(left: 1, right: 1),
        width: 7,
        height: 7,
        decoration: BoxDecoration(shape: BoxShape.circle, color: Colors.red),
      );
      Widget green = Container(
        width: 7,
        height: 7,
        decoration: BoxDecoration(shape: BoxShape.circle, color: Colors.green),
      );

      if(types.containsKey(d)) {
        final data = types[d];
        l = data.length;
        if(l == 1) {
          if(!data.contains(Constant.TYPE_IN)) {
            w.add(red);
          }else{
            w.add(green);
          }
        }else if(l > 1) {
          w.add(red);
          w.add(green);
        }
      }
      return Row(mainAxisAlignment: MainAxisAlignment.center, children: w);
    }

    Widget dateTileBuilder(date, selectedDate, rowIndex, dayName, isDateMarked, isDateOutOfRange, CalendarLoad state) {
      final formatter = DateFormat('yyyy-MM-dd');

      bool isSelectedDate = formatter.format(date) == formatter.format(state.selectedDate);
      Color fontColor = isDateOutOfRange ? Colors.black26 : Colors.black87;
      TextStyle normalStyle = TextStyle(fontSize: 17, fontWeight: FontWeight.w800, color: fontColor);
      TextStyle selectedStyle = TextStyle(fontSize: 17, fontWeight: FontWeight.w800, color: Colors.black87);
      TextStyle dayNameStyle = TextStyle(fontSize: 14.5, color: fontColor);
      List<Widget> _children = [
        Text(dayName, style: dayNameStyle),
        Text(date.day.toString(), style: !isSelectedDate ? normalStyle : selectedStyle),
      ];

      if (isDateMarked == true) {
        _children.add(getMarkedIndicatorWidget(state, date));
      }

      return AnimatedContainer(
        duration: Duration(milliseconds: 150),
        alignment: Alignment.center,
        padding: EdgeInsets.only(top: 8, left: 5, right: 5, bottom: 5),
        decoration: BoxDecoration(
          color: !isSelectedDate ? Colors.transparent : Colors.white70,
          borderRadius: BorderRadius.all(Radius.circular(60)),
        ),
        child: Column(
          children: _children,
        ),
      );
    }
    
    return Scaffold(
      // appBar: AppBar(
      //   title: Container(height: 0.0),
      // ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.of(context).push(
            pageRoute(FormExpense())
          );
        },
        tooltip: 'Add',
        child: Icon(Icons.add),
      ),
      body: MultiBlocListener(
        listeners: [
          BlocListener<InputexpenseBloc, InputexpenseState>(
            bloc:_inputBloc,
            listener: (context, state) {
              
              if(state is InputexpenseSuccess) {
                // Navigator.of(context).pop();
                _calendarBloc..add(OnCalendarLoad(
                  selectedDate: null
                ));
              }
              
            },
          ),
          BlocListener<CalendarBloc, CalendarState>(
            bloc:_calendarBloc,
            listener: (context, state) {
              if(state is CalendarLoad) {
                _calendarItemBloc..add(OnCalendarItemLoad(date: state.selectedDate));
              }
            },
          ),
        ],
        child: Container(
          child: Column(
            children: <Widget>[
              SizedBox(height: 25.0),
              BlocBuilder<CalendarBloc, CalendarState>(
                bloc: _calendarBloc,
                builder: (context, state) {
                    
                  if(state is CalendarLoad) {
                    
                    return CalendarStrip(
                      startDate: state.startDate,
                      endDate: state.endDate,
                      selectedDate: state.selectedDate,
                      // addSwipeGesture: true,
                      onDateSelected: onSelect,
                      dateTileBuilder: (date, selectedDate, rowIndex, dayName, isDateMarked, isDateOutOfRange) {                        
                        return dateTileBuilder(date, selectedDate, rowIndex, dayName, isDateMarked, isDateOutOfRange, state);
                      },
                      iconColor: Colors.black87,
                      monthNameWidget: _monthNameWidget,
                      markedDates: state.markedDates,
                      containerDecoration: BoxDecoration(color: Colors.black12),
                    );
                  }

                  return Container(height: 0.0,);
                },
              ),
              BlocBuilder<CalendarItemBloc, CalendarItemState>(
                bloc: _calendarItemBloc,
                builder: (context, state) {

                  if(state is CalendarItemLoaded) {

                    if(state.items.length == 0) {
                      return Expanded(
                        child: Container(
                          child: ListTile(
                            leading: Icon(Icons.sentiment_dissatisfied),
                            title: Text("Empty Result ?"),
                            subtitle: Text("Try adding some data first."),
                          )
                        ),
                      );
                    }

                    return Expanded(
                      child: ListView(
                        padding: EdgeInsets.all(10.0),
                        children: state.items.map((model) {
                          return Dismissible(
                            background: slideRightBackground(),
                            secondaryBackground: slideLeftBackground(),
                            child: InkWell(
                              onTap: () {

                              },
                              child: ListTile(
                                leading: Icon(model.type == Constant.TYPE_IN ? Icons.attach_money : Icons.shopping_cart),
                                title: Text(model.title.toString()),
                                subtitle: Text(model.content.toString()),
                                trailing: Text(model.price.toString()),
                              ),
                            ),
                            key: Key("dissmiss-${model.id}"),
                            confirmDismiss: (direction) async {
                              if (direction == DismissDirection.endToStart) {
                                final bool res = await showDialog(
                                    context: context,
                                    builder: (BuildContext context) {
                                      return AlertDialog(
                                        title: Text("Remove item?"),
                                        content: Text(
                                            "${model.content} will remove permantently"),
                                        actions: <Widget>[
                                          FlatButton(
                                            child: Text(
                                              "Cancel",
                                              // style: TextStyle(color: Colors.black),
                                            ),
                                            onPressed: () {
                                              Navigator.of(context).pop();
                                            },
                                          ),
                                          FlatButton(
                                            child: Text(
                                              "Delete",
                                              style: TextStyle(color: Colors.red),
                                            ),
                                            onPressed: () {
                                              BlocProvider.of<InputexpenseBloc>(context)..add(OnRemoveItem(id: model.id));
                                              Navigator.of(context).pop();
                                            },
                                          ),
                                        ],
                                      );
                                    });
                                    
                              } else {
                                Navigator.of(context).push(
                                  pageRoute(UpdateExpense(model: model))
                                );
                              }
                              return false;
                            },
                          );
                        }).toList(),
                      )
                    );
                  }

                  return Expanded(
                    child: Container(
                      child: Center(
                        child: CircularProgressIndicator(),
                      ),
                    ),
                  );
                },
              )
            ],
          ),
        )
      )     
    );
  }

  Widget slideRightBackground() {
    return Container(
      color: Colors.green,
      child: Align(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            SizedBox(
              width: 20,
            ),
            Icon(
              Icons.edit,
              color: Colors.white,
            ),
            Text(
              " Edit",
              style: TextStyle(
                color: Colors.white,
                fontWeight: FontWeight.w700,
              ),
              textAlign: TextAlign.left,
            ),
          ],
        ),
        alignment: Alignment.centerLeft,
      ),
    );
  }

  Widget slideLeftBackground() {
    return Container(
      color: Colors.red,
      child: Align(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: <Widget>[
            Icon(
              Icons.delete,
              color: Colors.white,
            ),
            Text(
              " Delete",
              style: TextStyle(
                color: Colors.white,
                fontWeight: FontWeight.w700,
              ),
              textAlign: TextAlign.right,
            ),
            SizedBox(
              width: 20,
            ),
          ],
        ),
        alignment: Alignment.centerRight,
      ),
    );
  }
}
