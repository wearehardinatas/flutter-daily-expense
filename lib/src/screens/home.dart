import 'dart:math';

import 'package:expenselist/src/bloc/charts_bloc.dart';
import 'package:expenselist/src/screens/form_expense.dart';
import 'package:expenselist/src/utils/pageroute.dart';
import 'package:flutter/material.dart';
import 'package:charts_flutter/flutter.dart' as charts;
import 'package:flutter_bloc/flutter_bloc.dart';



class HomePage extends StatelessWidget {

  Widget _loadChart(BuildContext context, ChartsBloc bloc) {
    return Container(
      child: Column(
        children: <Widget>[
          
          // SizedBox(height: 25.0),
          Container(
            height: 200.0,
            child: Card(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(15.0),
              ),
              elevation: 5.0,
              margin: EdgeInsets.all(10.0),
              child: BlocBuilder<ChartsBloc, ChartsState>(
                bloc: bloc,
                builder: (context, state) {
                  Widget _barChart = Container(height: 0.0,);
                  
                  if(state is ChartLoading) {
                    return Center(
                      child: CircularProgressIndicator(),
                    );
                  }

                  if(state is ChartLoaded) {
                    return Padding(
                      padding: EdgeInsets.all(10.0),
                      child: charts.BarChart(
                        state.bar,
                        animate: true,
                        barGroupingType: charts.BarGroupingType.grouped,
                        behaviors: [new charts.SeriesLegend()],
                        domainAxis: charts.OrdinalAxisSpec(
                          renderSpec: charts.SmallTickRendererSpec(
                            labelStyle: charts.TextStyleSpec(
                              fontSize: 12, // size in Pts.
                              color: charts.MaterialPalette.black
                            ),
                            lineStyle: charts.LineStyleSpec(
                              color: charts.MaterialPalette.black
                            )
                          )
                        ),
                        primaryMeasureAxis: charts.NumericAxisSpec(
                          renderSpec: charts.GridlineRendererSpec(
                            labelStyle: charts.TextStyleSpec(
                              fontSize: 12, // size in Pts.
                              color: charts.MaterialPalette.black
                            ),
                            lineStyle: charts.LineStyleSpec(
                              color: charts.MaterialPalette.black
                            )
                          )
                        ),
                      )
                    );
                  }
                  return _barChart;
                },
              ),
            ),
          ),
          Container(
            height: 200.0,
            child: GridView.count(
              crossAxisCount: 2,
              childAspectRatio: 1.0,
              padding: EdgeInsets.all(2.0),
              mainAxisSpacing: 2.0,
              crossAxisSpacing: 2.0,
              children: <Widget>[
                GridTile(
                  child: Card(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(15.0),
                    ),
                    elevation: 5.0,
                    margin: EdgeInsets.all(10.0),
                    child: BlocBuilder<ChartsBloc, ChartsState>(
                      bloc: bloc,
                      builder: (context, state) {
                        Widget _gaugeDailyChart = Center(
                          child: CircularProgressIndicator(),
                        );

                        if(state is ChartLoaded) {
                          if (state.pieMonthly == null) return _gaugeDailyChart;
                          return charts.PieChart(
                            state.pieMonthly,
                            animate: true,
                            behaviors: [
                              charts.DatumLegend(
                                // Positions for "start" and "end" will be left and right respectively
                                // for widgets with a build context that has directionality ltr.
                                // For rtl, "start" and "end" will be right and left respectively.
                                // Since this example has directionality of ltr, the legend is
                                // positioned on the right side of the chart.
                                position: charts.BehaviorPosition.bottom,
                                // For a legend that is positioned on the left or right of the chart,
                                // setting the justification for [endDrawArea] is aligned to the
                                // bottom of the chart draw area.
                                outsideJustification: charts.OutsideJustification.endDrawArea,
                                // By default, if the position of the chart is on the left or right of
                                // the chart, [horizontalFirst] is set to false. This means that the
                                // legend entries will grow as new rows first instead of a new column.
                                horizontalFirst: false,
                                // By setting this value to 2, the legend entries will grow up to two
                                // rows before adding a new column.
                                desiredMaxRows: 2,
                                // This defines the padding around each legend entry.
                                cellPadding: new EdgeInsets.only(right: 2.0, bottom: 2.0),
                                // Render the legend entry text with custom styles.
                                entryTextStyle: charts.TextStyleSpec(
                                    color: charts.MaterialPalette.purple.shadeDefault,
                                    fontFamily: 'Georgia',
                                    fontSize: 11),
                              )
                            ],
                            defaultRenderer: new charts.ArcRendererConfig(
                              arcWidth: 30, startAngle: 4 / 5 * pi, arcLength: 7 / 5 * pi
                            )
                          );
                        }
                        
                        return _gaugeDailyChart;
                      },
                    )
                  ),
                ),
                GridTile(
                  child: Card(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(15.0),
                    ),
                    elevation: 5.0,
                    margin: EdgeInsets.all(10.0),
                    child: BlocBuilder<ChartsBloc, ChartsState>(
                      bloc: bloc,
                      builder: (context, state) {
                        Widget _gaugeDailyChart = Center(
                          child: CircularProgressIndicator(),
                        );

                        if(state is ChartLoaded) {
                          if (state.pieDaily == null) return _gaugeDailyChart;
                          return charts.PieChart(
                            state.pieDaily,
                            animate: true,
                            behaviors: [
                              charts.DatumLegend(
                                // Positions for "start" and "end" will be left and right respectively
                                // for widgets with a build context that has directionality ltr.
                                // For rtl, "start" and "end" will be right and left respectively.
                                // Since this example has directionality of ltr, the legend is
                                // positioned on the right side of the chart.
                                position: charts.BehaviorPosition.bottom,
                                // For a legend that is positioned on the left or right of the chart,
                                // setting the justification for [endDrawArea] is aligned to the
                                // bottom of the chart draw area.
                                outsideJustification: charts.OutsideJustification.endDrawArea,
                                // By default, if the position of the chart is on the left or right of
                                // the chart, [horizontalFirst] is set to false. This means that the
                                // legend entries will grow as new rows first instead of a new column.
                                horizontalFirst: false,
                                // By setting this value to 2, the legend entries will grow up to two
                                // rows before adding a new column.
                                desiredMaxRows: 2,
                                // This defines the padding around each legend entry.
                                cellPadding: new EdgeInsets.only(right: 2.0, bottom: 2.0),
                                // Render the legend entry text with custom styles.
                                entryTextStyle: charts.TextStyleSpec(
                                    color: charts.MaterialPalette.purple.shadeDefault,
                                    fontFamily: 'Georgia',
                                    fontSize: 11),
                              )
                            ],
                            defaultRenderer: new charts.ArcRendererConfig(
                              arcWidth: 30, startAngle: 4 / 5 * pi, arcLength: 7 / 5 * pi
                            )
                          );
                        }
                        
                        return _gaugeDailyChart;
                      },
                    )
                  ),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    
    ChartsBloc _chartsBloc = BlocProvider.of<ChartsBloc>(context);

    return Scaffold(
      body: BlocConsumer<ChartsBloc, ChartsState>(
        bloc: _chartsBloc ..add(OnChartLoad()),
        listener: (context, state) {
          if(state is ChartFailure) {
            Scaffold.of(context).showSnackBar(
              SnackBar(
                content: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Icon(Icons.error_outline),
                    Expanded(
                      child: Text(state.error, maxLines: 4,
                        overflow: TextOverflow.ellipsis,
                        textDirection: TextDirection.ltr,
                        textAlign: TextAlign.left
                      ),
                    )
                  ],
                ),
                backgroundColor: Colors.red,
                behavior: SnackBarBehavior.floating,
              ),
            );
          }
        },
        builder: (context, state) {
          return ListView(
            children: <Widget>[
              _loadChart(context, _chartsBloc)
            ],
          );
        },
      ),
    );
  }
}
