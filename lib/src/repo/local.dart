import 'package:expenselist/src/models/chart_pie.dart';
import 'package:expenselist/src/models/chart_stacked.dart';
import 'package:expenselist/src/models/expence.dart';
import 'package:expenselist/src/utils/constant.dart';
import 'package:expenselist/src/utils/db.dart';
import 'package:expenselist/src/utils/exceptions.dart';
import 'package:intl/intl.dart';
import 'package:sqflite/sqflite.dart';

class LocalRepo {
  static const String TABLENAME = 'expense';
  DBHelper dbHelper = DBHelper();

  Future<void> insertExpense(ExpenceModel model) async {
    final Database db = await dbHelper.database;
    int save = await db.transaction((Transaction tx) async {
      return await tx.insert(TABLENAME, model.toJson());
    });
    // await db.close();

    if(save <= 0) throw SqliteException("Failed to save data.");
    
  }

  Future<void> updateExpense(ExpenceModel model) async {
    final Database db = await dbHelper.database;
    int save = await db.transaction((Transaction tx) async {
      return await tx.update(TABLENAME, model.toJson(), where: 'id = ?', whereArgs: [model.id]);
    });

    if(save <= 0) throw SqliteException("Failed to update data.");
  }

  Future<int> countAllExpense() async {
    final Database db = await dbHelper.database;
    int count = Sqflite.firstIntValue(await db.rawQuery("SELECT COUNT(*) FROM $TABLENAME"));
    // await db.close();
    return count;
  }

  Future<List<ExpenceModel>> allexpense() async {
    final Database db = await dbHelper.database;
    final List<Map<String, dynamic>> maps = await db.query("$TABLENAME");
    // await db.close();

    return List.generate(maps.length, (i) {
      return ExpenceModel(
        id: maps[i]['id'],
        title: maps[i]['title'],
        content: maps[i]['content'],
        price: maps[i]['price'].round(),
        date: maps[i]['date'],
        type: maps[i]['type']
      );
    });
  }

  Future<List<ExpenceModel>> getMarkedItemsByDate(String date) async {
    final Database db = await dbHelper.database;
    final sql = 'SELECT * FROM $TABLENAME WHERE date = ?';
    final List<Map<String, dynamic>> maps = await db.rawQuery(sql, [date]);
    // await db.close();

    return List.generate(maps.length, (i) {
      return ExpenceModel(
        id: maps[i]['id'],
        title: maps[i]['title'],
        content: maps[i]['content'],
        price: maps[i]['price'].round(),
        date: maps[i]['date'],
        type: maps[i]['type']
      );
    });
  }

  Future<Map<String, List<String>>> getMarkedTypes() async {
    final Database db = await dbHelper.database;
    final sql = 'SELECT date, type FROM $TABLENAME GROUP BY date, type';
    final List<Map<String, dynamic>> maps = await db.rawQuery(sql);

    Map<String, List<String>> map = {};
    maps.forEach((m) {
      if(!map.containsKey(m['date'])) {
        map.putIfAbsent(m['date'], ()=>List<String>());
      }
      map[m['date']].add(m['type']);
    });

    return map;
  }

  Future<Map<String, List<ChartStackedModel>>> getYearlyStackedChart(String year) async {
    final Database db = await dbHelper.database;
    final sql = 'SELECT SUM(price) total, strftime(\'%m\', date) m, type FROM $TABLENAME WHERE strftime(\'%Y\', date) = ? GROUP BY type, strftime(\'%m\', date)';
    final List<Map<String, dynamic>> maps = await db.rawQuery(sql, [year]);

    Map<String, List<ChartStackedModel>> map = {};
    maps.forEach((m) {

      if(!map.containsKey(m['type'])) {
        map.putIfAbsent(m['type'], ()=>List<ChartStackedModel>());
      }

      var formatter = DateFormat("MMMM");
      var month = formatter.format(DateTime(int.parse(year), int.parse(m['m'])));
      map[m['type']].add(ChartStackedModel(month: month.toString(), value: m['total'].round()));
    });

    return map;
  }

  Future<Map<String, ChartPieModel>> getMonthlyPieChart(DateTime date) async {
    final String m = date.month <= 9 ? '0' + date.month.toString() : date.month;
    final Database db = await dbHelper.database;
    final sql = 'SELECT SUM(price) total, strftime(\'%m\', date) m, type FROM $TABLENAME WHERE strftime(\'%m\', date) = ? GROUP BY type, strftime(\'%m\', date)';
    final List<Map<String, dynamic>> maps = await db.rawQuery(sql, [m]);

    Map<String, ChartPieModel> map = {};

    map.putIfAbsent(Constant.TYPE_IN, ()=>ChartPieModel(segment: Constant.TYPE_IN, value: 0));
    map.putIfAbsent(Constant.TYPE_OUT, ()=>ChartPieModel(segment: Constant.TYPE_OUT, value: 0));

    int inc = 0;
    int out = 0;
    if(maps.length == 1) {
      Map<String, dynamic> first = maps.first;

      if(first['type'] == Constant.TYPE_IN) {
        inc = first['total'].round();
        out = 0;
      }

      if(first['type'] == Constant.TYPE_OUT) {
        inc = 0;
        out = first['total'].round();
      }

    }else if(maps.length == 2) {
      Map<String, dynamic> first = maps.first;
      Map<String, dynamic> last = maps.last;


      if(first['type'] == Constant.TYPE_IN) {
        inc = first['total'].round() - last['total'].round();
        out = last['total'].round();
      }

      if(last['type'] == Constant.TYPE_IN) {
        inc = last['total'].round() - first['total'].round();
        out = last['total'].round();
      }

    }
    map[Constant.TYPE_IN] = ChartPieModel(segment: Constant.TYPE_IN, value: inc);
    map[Constant.TYPE_OUT] = ChartPieModel(segment: Constant.TYPE_OUT, value: out);

    return map;
  }

  Future<Map<String, ChartPieModel>> getDailyPieChart(DateTime date) async {
    final DateFormat formatter = DateFormat('yyyy-MM-dd');
    final String d = formatter.format(date).toString();
    final Database db = await dbHelper.database;
    final sql = 'SELECT SUM(price) total, date d, type FROM $TABLENAME WHERE date = ? GROUP BY type, date';
    final List<Map<String, dynamic>> maps = await db.rawQuery(sql, [d]);
    print("DatePieDailySeries: $maps");

    Map<String, ChartPieModel> map = {};

    map.putIfAbsent(Constant.TYPE_IN, ()=>ChartPieModel(segment: Constant.TYPE_IN, value: 0));
    map.putIfAbsent(Constant.TYPE_OUT, ()=>ChartPieModel(segment: Constant.TYPE_OUT, value: 0));

    int inc = 0;
    int out = 0;
    
    if(maps.length == 1) {
      Map<String, dynamic> first = maps.first;

      if(first['type'] == Constant.TYPE_IN) {
        inc = first['total'].round();
        out = 0;
      }

      if(first['type'] == Constant.TYPE_OUT) {
        inc = 0;
        out = first['total'].round();
      }

    }else if(maps.length == 2) {
      Map<String, dynamic> first = maps.first;
      Map<String, dynamic> last = maps.last;


      if(first['type'] == Constant.TYPE_IN) {
        inc = first['total'].round() - last['total'].round();
        out = last['total'].round();
      }

      if(last['type'] == Constant.TYPE_IN) {
        inc = last['total'].round() - first['total'].round();
        out = first['total'].round();
      }

    }

    map[Constant.TYPE_IN] = ChartPieModel(segment: Constant.TYPE_IN, value: inc);
    map[Constant.TYPE_OUT] = ChartPieModel(segment: Constant.TYPE_OUT, value: out);

    return map;
  }

  Future<List<DateTime>> getMarkedDates() async {
    final Database db = await dbHelper.database;
    final sql = 'SELECT strftime(\'%Y\', date) y, strftime(\'%m\', date) m, strftime(\'%d\', date) d FROM $TABLENAME GROUP BY date';
    final List<Map<String, dynamic>> maps = await db.rawQuery(sql);
    // await db.close();

    return List.generate(maps.length, (i) {
      return DateTime(
        int.parse(maps[i]['y']),
        int.parse(maps[i]['m']),
        int.parse(maps[i]['d'])
      );
    });
  }

  Future<List<ExpenceModel>> paging(int page, int limit) async {
    final Database db = await dbHelper.database;
    final sql = 'SELECT * FROM $TABLENAME OFFSET $page LIMIT $limit';
    final List<Map<String, dynamic>> maps = await db.rawQuery(sql);
    // await db.close();

    return List.generate(maps.length, (i) {
      return ExpenceModel(
        id: maps[i]['id'],
        title: maps[i]['title'],
        content: maps[i]['content'],
        price: maps[i]['price'].round(),
        date: maps[i]['date'],
        type: maps[i]['type']
      );
    });
  }

  Future<void> clearExpense() async {
    final Database db = await dbHelper.database;
    int delete = await db.transaction((Transaction tx) async {
      return await tx.rawDelete("DELETE FROM $TABLENAME");
    });

    if(delete <= 0) throw SqliteException("Failed to delete data in an empty storage.");
  }

  Future<void> clearExpenseById(int id) async {
    final Database db = await dbHelper.database;
    int delete = await db.transaction((Transaction tx) async {
      return await tx.rawDelete("DELETE FROM $TABLENAME WHERE id = ?", [id]);
    });

    if(delete <= 0) throw SqliteException("Failed to delete data in an empty storage.");
  }
}